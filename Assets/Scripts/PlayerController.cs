﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float topSpeed = 10f;
    bool FacingRight = true;
    public bool Grounded = false;

    public Transform groundCheck;

    float GroundRadious = 0.2f;

    public float JumpForse = 700f;

    public LayerMask WhatIsGround;
    // Use this for initialization
    Animator anim;


	void Start () {
        anim = GetComponent<Animator>();
	}



	
	// Update is called once per frame
	void FixedUpdate () {
        Grounded = Physics2D.OverlapCircle(groundCheck.position, GroundRadious, WhatIsGround);

        anim.SetBool("Ground", Grounded);
        anim.SetFloat("vSpeed", GetComponent<Rigidbody2D>().velocity.y);

        float move = Input.GetAxis("Horizontal");

        GetComponent<Rigidbody2D>().velocity = new Vector2(move * topSpeed, GetComponent<Rigidbody2D>().velocity.y);
        anim.SetFloat("Speed", Mathf.Abs(move));
        if (move > 0 && !FacingRight) Flip();
        else if (move < 0 && FacingRight) Flip();

    }

    void Update()
    {
        if(Grounded && Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetBool("Ground", false);
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, JumpForse));
        }
        
    }



    void Flip()
    {
        FacingRight = !FacingRight;
        Vector3 theScale = transform.localScale;

        theScale.x *= -1;
        transform.localScale = theScale;

    }


  // void OnCollisionEnter2D(Collision2D coll)
    //{
      //  if (coll.gameObject.tag == "ChangeScene1")
          //  SceneManager.LoadScene("AnimationScene2");
        //if (coll.gameObject.tag == "ChangeScene2")
       //     SceneManager.LoadScene("AnimationScene3");

    //}


}
