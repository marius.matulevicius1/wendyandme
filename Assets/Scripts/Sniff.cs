﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Sniff : MonoBehaviour {
    public GameObject directionObject;
    // Use this for initialization
    Animator anim;
    public AudioSource sniffSound;
    public AudioSource digSound;
    float distance;
    public GameObject Amulet;
    public Rigidbody2D rig;
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        //-----------------SNIF
        if (Input.GetKey(KeyCode.LeftControl))
        {
            sniffSound.Play();
            StartCoroutine("wait");
        }

        //------------------DIG
        if (Input.GetKey(KeyCode.S)) {
            digSound.Play();
            anim.SetBool("Dig", true);
            
            distance = Vector3.Distance(transform.position, Amulet.transform.position);

            if(distance < 10)
            {
                Amulet.SetActive(true);
              //  StartCoroutine("ChangeScene");
            }
            StartCoroutine("wait2");

        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            

        }
	}
     IEnumerator wait()
    {
        directionObject.SetActive(true);
        if(transform.position.x > -132 && directionObject.transform.localScale.x >0)
        {

            Vector3 temp = new Vector3 (-1f,1f,1f);
            directionObject.transform.localScale = Vector3.Scale(directionObject.transform.localScale, temp) ;
        }
        if (transform.position.x < -132 && directionObject.transform.localScale.x < 0)
        {
            Vector3 temp = new Vector3(-1f, 1f, 1f);
            directionObject.transform.localScale = Vector3.Scale(directionObject.transform.localScale, temp);
        }
            yield return new WaitForSeconds(2f);
        directionObject.SetActive(false);
    }

    IEnumerator wait2()
    {
        yield return new WaitForSeconds(1f);
        anim.SetBool("Dig", false);
    }

    IEnumerator ChangeScene()
    {
        yield return new WaitForSeconds(4f);
        SceneManager.LoadScene("AnimationScene2");
    }

}
