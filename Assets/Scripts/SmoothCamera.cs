﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothCamera : MonoBehaviour {
    public Transform target;
    Camera MyCam;
    public float Yskaicius = 0.5f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (target)
            transform.position = Vector3.Lerp(transform.position, target.position, 0.1f) + new Vector3(0f,Yskaicius, -10f);
	}
}
