﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpScript : MonoBehaviour {
    Animator anim;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            anim.SetBool("ShiftHold", true);
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            anim.SetBool("ShiftHold", false);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetBool("Jump", true);
            StartCoroutine("wait");
        }
    }

        IEnumerator wait() {
            yield return new WaitForSeconds(0.5f);
            anim.SetBool("Jump", false);
        }


	}

