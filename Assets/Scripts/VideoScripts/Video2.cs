﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]

public class Video2 : MonoBehaviour
{

    public MovieTexture CutScene2;
    public AudioSource audio;
    public RawImage Raw;
    public GameObject canvas;
    public GameObject amuletas;
    public GameObject Music;
    public GameObject EpicMusic;
    // Use this for initialization
    void Start()
    {
        StartCoroutine("wait1");
    }

    // Update is called once per frame

        IEnumerator wait1()
    {
        yield return new WaitForSeconds(2f);
        canvas.SetActive(true);
        Raw.texture = CutScene2 as MovieTexture;
        audio.clip = CutScene2.audioClip;
        Music.SetActive(false);
        CutScene2.Play();
        audio.Play();
        StartCoroutine("wait");
    }


    IEnumerator wait()
    {
        
        yield return new WaitForSeconds(8.5f);
        EpicMusic.SetActive(true);
        canvas.SetActive(false);
        amuletas.SetActive(false);
    }
}