﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]

public class Video1 : MonoBehaviour {
    public MovieTexture movie;
    public AudioSource audio;
    // Use this for initialization
    void Start () {
        GetComponent<RawImage>().texture = movie as MovieTexture;
        audio.clip = movie.audioClip;
        movie.Play();
        audio.Play();

        StartCoroutine("wait");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    IEnumerator wait()
    {
        yield return new WaitForSeconds(9.5f);
        SceneManager.LoadScene("GameScene1");
    }
}
