﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
[RequireComponent(typeof(AudioSource)) ]

public class Video3 : MonoBehaviour {

    // Use this for initialization
    public MovieTexture movie;
    public AudioSource audio;
    public GameObject Press;


	void Start () {
        GetComponent<RawImage>().texture = movie as MovieTexture;
        audio.clip = movie.audioClip;
        StartCoroutine("wait");
        movie.Play();
        audio.Play();
        
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Press.SetActive(false);
            movie.Play();
            audio.Play();
        }
	}
    IEnumerator wait()
    {
        yield return new WaitForSeconds(13f);
        movie.Pause();
        audio.Pause();
        Press.SetActive(true);
        
    }

   
}
