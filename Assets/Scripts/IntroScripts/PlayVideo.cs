﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class PlayVideo : MonoBehaviour {
    public MovieTexture mov;
    public AudioSource audio;
    // Use this for initialization
    void Start () {
        audio.clip = mov.audioClip;
        mov.Play();
        audio.Play();
        mov.loop = true;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
